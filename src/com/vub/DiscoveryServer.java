package com.vub;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;

/**
 * Created by adrienschautteet on 29/05/17.
 */
public class DiscoveryServer implements PeerDiscoveryProtocol {

    private List<ServerHTTProtocol> connectedPeers = new ArrayList<>();

    public DiscoveryServer() throws RemoteException {
        UnicastRemoteObject.exportObject(this, 0);
        // Register server in registry
        LocateRegistry.getRegistry().rebind("DiscoveryServer", this);
    }

    @Override
    public List<ServerHTTProtocol> getConnectedPeers() throws RemoteException {
        return connectedPeers;
    }

    @Override
    public void addPeer(ServerHTTProtocol peer) throws RemoteException, NotBoundException {
        this.connectedPeers.add(peer);
    }

    @Override
    public void notifyNetwork(ServerHTTProtocol sender) throws RemoteException, NotBoundException {
        for (ServerHTTProtocol peer : connectedPeers) {
            peer.updatePeerInfo();
        }
    }

    @Override
    public ServerHTTProtocol discoverPeer() throws RemoteException {
        return connectedPeers.get(0);
    }

    @Override
    public ServerHTTProtocol getPeerWithFreeSpaceForBusiness(ServerHTTProtocol sender, String bid) throws RemoteException {
        for (ServerHTTProtocol peer : connectedPeers) {
            if (!peer.isHostingBusinessData(bid) && !peer.equals(sender)) {
                return peer;
            }
        }
        return null;
    }

    public static void main(String[] args) throws RemoteException, NotBoundException {
        new DiscoveryServer();
    }
}
