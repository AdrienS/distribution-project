package com.vub;

import com.vub.pdproject.data.models.Review;
import com.vub.pdproject.data.models.Business;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;
import java.util.Scanner;

/**
 * Created by adrienschautteet on 11/05/17.
 */
public class HTTPClient extends UnicastRemoteObject implements ClientHTTProtocol {

    private PeerDiscoveryProtocol discoveryServer;
    private ServerHTTProtocol connectedPeer;

    public HTTPClient() throws RemoteException, NotBoundException {
        // Register server in registry
        LocateRegistry.getRegistry().rebind("HTTPClient", this);

        // Get the registry and lookup our discovery server
        Registry reg = LocateRegistry.getRegistry();
        discoveryServer = (PeerDiscoveryProtocol) reg.lookup("DiscoveryServer");

        // Connect to a peer
        connectedPeer = discoveryServer.discoverPeer();
    }

    public void sendMessage(String[] args) throws RemoteException, NotBoundException {
        // Discover a peer and send a message to it
        connectedPeer.receiveMessge(args);
    }

    @Override
    public void receiveBusinessResponse(List<Business> results) throws RemoteException {
        for (Business business : results) {
            System.out.println("---- "+business.name);
        }
    }

    @Override
    public void receiveInfoResponse(Business result, List<Review> reviews) throws RemoteException {
        System.out.println("---- "+result.name);
        System.out.println("---- Stars: "+result.stars);
        System.out.println("---- Address: "+result.address);
        System.out.println("---- City: "+result.city);
        for (Review review : reviews) {
            try {
                System.out.println("-------- Review stars: " + review.stars);
                System.out.println("------------ Review Text: " + review.text);
            } catch (NullPointerException e) {
                System.out.println("Heres the bloody bugger.");
            }
        }
    }

    public static void main(String[] args) throws RemoteException, NotBoundException {
        Scanner scanner = new Scanner(System.in);

        System.out.print("##### Connected to Yelp Interface ####\n");

        String request = "";

        while (!request.equals("exit")) {
            System.out.print("Enter command: ");
            request = scanner.nextLine();
            String[] message = request.split(" ");

            new HTTPClient().sendMessage(message);
        }
    }

}
