package com.vub;

import com.vub.pdproject.data.YelpData;
import com.vub.pdproject.data.models.Business;
import com.vub.pdproject.data.models.Review;

import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Map;

/**
 * Created by adrienschautteet on 11/05/17.
 */
public interface ServerHTTProtocol extends Remote {
    // HTTP operations
    void receiveMessge(String[] msg) throws RemoteException;
    void GET(String[] msg) throws RemoteException, NotBoundException;
    void POST(String[] msg) throws RemoteException, NotBoundException;

    // Peer operations
    void updatePeerInfo() throws RemoteException, NotBoundException;
    void insertPointerTable(String bid, ServerHTTProtocol peer) throws RemoteException;
    boolean isHostingBusinessData(String bid) throws RemoteException;
    List<Business> getBusinessResults(String keyword) throws RemoteException;
    List<Review> getReviewResults(String businessId) throws RemoteException;
    void executePost(Business business, Review review) throws RemoteException, NotBoundException;

    // Vault operations
    void updateVaults(YelpData vault, List<Business> businesses, List<Review> reviews) throws RemoteException;
    List<Business> getBusinessVault() throws RemoteException;
}
