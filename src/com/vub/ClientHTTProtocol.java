package com.vub;

import com.vub.pdproject.data.models.Business;
import com.vub.pdproject.data.models.Review;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/**
 * Created by adrienschautteet on 11/05/17.
 */
public interface ClientHTTProtocol extends Remote{
    void receiveBusinessResponse(List<Business> results) throws RemoteException;
    void receiveInfoResponse(Business result, List<Review> reviews) throws RemoteException;
}
