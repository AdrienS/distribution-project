package com.vub;

import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/**
 * Created by adrienschautteet on 29/05/17.
 */
public interface PeerDiscoveryProtocol extends Remote {

    List<ServerHTTProtocol> getConnectedPeers() throws RemoteException;
    void addPeer(ServerHTTProtocol peer) throws RemoteException, NotBoundException;
    void notifyNetwork(ServerHTTProtocol sender) throws RemoteException, NotBoundException;
    ServerHTTProtocol discoverPeer() throws  RemoteException;
    ServerHTTProtocol getPeerWithFreeSpaceForBusiness(ServerHTTProtocol sender, String bid) throws RemoteException;
}
