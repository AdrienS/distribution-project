package com.vub;

import com.vub.pdproject.data.YelpData;
import com.vub.pdproject.data.models.Business;
import com.vub.pdproject.data.models.Review;
import com.vub.pdproject.search.ParallelSearch;
import com.vub.pdproject.search.QueryEngine;
import com.vub.pdproject.search.YelpQuery;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;

/**
 * Created by adrienschautteet on 11/05/17.
 */
public class YelpInterface implements ServerHTTProtocol {

    // Peer network info
    private List<ServerHTTProtocol> connectedPeers = new ArrayList<>();
    private int peerAmount = 0;
    private Map<String, ServerHTTProtocol> businessPeerPointerTable = new HashMap<>();
    static int reviewCap = 2;

    // Fork/Join settings
    private final int AMOUNT_OF_WORKER_THREADS = 4;
    private final int THRESHOLD = 5000;

    // Data vaults
    static YelpData data = YelpData.forPreset(1);
    private YelpData vault;
    private List<Business> businesses = new ArrayList<>();
    private List<Review> reviews = new ArrayList<>();

    // Connected clients
    private ArrayList<ClientHTTProtocol> clients = new ArrayList<>();

    public YelpInterface() throws RemoteException, NotBoundException {
        UnicastRemoteObject.exportObject(this, 0);
        // Register server in registry
        LocateRegistry.getRegistry().rebind("YelpInterface", this);

        // Contact discovery server
        Registry reg = LocateRegistry.getRegistry();
        PeerDiscoveryProtocol discoveryServer = (PeerDiscoveryProtocol) reg.lookup("DiscoveryServer");
        // Add self to peer network
        discoveryServer.addPeer(this);
        // Notify the network with update
        discoveryServer.notifyNetwork(this);

        // Populate business and review vault
        if (peerAmount < 2) {
            for (String bid : data.getBusinessIDs()) {
                Business original = data.getBusiness(bid);
                Business custom = new Business(bid, original.name, original.address, original.city, original.stars);
                businesses.add(custom);
            }
            vault = new YelpData(businesses, reviews);
        }
    }
    // ~~~~~~~~~~~~~~~ HTTP operations ~~~~~~~~~~~~~~~
    @Override
    public void receiveMessge(String[] msg) throws RemoteException {
        try {
            switch (msg[0]) {
                case "GET":
                    GET(msg);
                    break;
                case "POST":
                    POST(msg);
                    break;
                default:
                    System.out.println("> Message not understood: \"" + msg[0] + "\"");
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void GET(String[] msg) throws RemoteException, NotBoundException {
        // TODO: handle no arguments
        System.out.println("GET request send.");
        try {
            switch (msg[1]) {
                case "businesses":
                    processBusinessRequest(msg[2]);
                    break;
                case "info":
                    processInfoRequest(String.join(" ", Arrays.copyOfRange(msg, 2, msg.length)));
                    break;
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void POST(String[] msg) throws RemoteException, NotBoundException {
        System.out.println("POST request send.");
        String businessName = "";
        String review = "";
        for (int i = 1; i < msg.length; i++) {
            if (msg[i].equals("review:")) {
                businessName = String.join(" ", Arrays.copyOfRange(msg, 1, i));
                review = String.join(" ", Arrays.copyOfRange(msg, i+1, msg.length));
                processPostRequest(businessName, review);
                break;
            }
        }
    }

    // ~~~~~~~~~~~~~~~ Peer operations ~~~~~~~~~~~~~~~
    @Override
    public void updatePeerInfo() throws RemoteException, NotBoundException {
        // Contact discovery server
        PeerDiscoveryProtocol discoveryServer = contactDiscoveryServer();

        // Update
        this.connectedPeers = discoveryServer.getConnectedPeers();
        this.peerAmount = this.connectedPeers.size();
    }

    @Override
    public boolean isHostingBusinessData(String bid) throws RemoteException {
        return hasPeerLink(bid);
    }

    @Override
    public List<Business> getBusinessResults(String keyword) throws RemoteException {
        YelpQuery query;
        QueryEngine parallelSearch;
        List<QueryEngine.RRecord> queryResults;
        List<Business> results = new ArrayList<>();

        query = new YelpQuery(keyword, vault);
        parallelSearch = new ParallelSearch(AMOUNT_OF_WORKER_THREADS, THRESHOLD);
        queryResults = query.execute(parallelSearch);

        for (QueryEngine.RRecord res : queryResults) {
            results.add(vault.getBusiness(res.businessID));
        }
        return results;
    }

    @Override
    public List<Review> getReviewResults(String businessId) throws RemoteException {
        List<Review> results = new ArrayList<>();
        for (String bid : vault.getBusinessIDs()) {
            if (bid.equals(businessId)) {
                for (String rid : vault.getBusiness(bid).reviews)
                results.add(vault.getReview(rid));
                // Check if other peers have more data by looking up pointer
                if (businessPeerPointerTable.containsKey(bid)) {
                    ServerHTTProtocol peer = businessPeerPointerTable.get(bid);
                    for (Business b : peer.getBusinessVault()) {
                        if (b.id.equals(bid)) {
                            List<Review> peerResults = peer.getReviewResults(b.id);
                            results.addAll(peerResults);
                            break;
                        }
                    }
                }
            }
        }
        return results;
    }

    // ~~~~~~~~~~~~~~~ Vault operations ~~~~~~~~~~~~~~~
    @Override
    public void updateVaults(YelpData vault, List<Business> businesses, List<Review> reviews) throws RemoteException {
        this.businesses = businesses;
        this.reviews = reviews;
        this.vault = vault;
    }

    @Override
    public List<Business> getBusinessVault() throws RemoteException {
        return businesses;
    }

    @Override
    public void insertPointerTable(String bid, ServerHTTProtocol peer) throws RemoteException {
        this.businessPeerPointerTable.put(bid, peer);
    }

    @Override
    public void executePost(Business business, Review review) throws RemoteException, NotBoundException {
        if (reviews.size() < reviewCap) {
            String bid = business.id;
            if (vault == null || !isBusinessInVault(bid)) {
                business = new Business(bid, business.name, business.address, business.city, business.stars);
                business.updateReviews(review.id);
                businesses.add(business);
            } else {
                business = vault.getBusiness(bid);
                business.updateReviews(review.id);
                businesses.add(business);
            }

            reviews.add(review);
            YelpData update = new YelpData(businesses, reviews);
            updateVaults(update, businesses, reviews);
        } else if (businessPeerPointerTable.containsKey(business.id)) {
            ServerHTTProtocol peer = businessPeerPointerTable.get(business.id);
            peer.executePost(business, review);
        } else {
            PeerDiscoveryProtocol discovery = contactDiscoveryServer();
            ServerHTTProtocol peer = discovery.getPeerWithFreeSpaceForBusiness(this, business.id);
            if (peer != null) {
                insertPointerTable(business.id, peer);
                peer.executePost(business, review);
            } else {
                System.out.println("Network doesnt have enough space to save this review.");
            }
        }
    }

    public void processBusinessRequest(String keyword) throws RemoteException, NotBoundException {

        List<Business> results = new ArrayList<>();
        // Check in own vault
        results.addAll(getBusinessResults(keyword));

        // Loop over peers and check their vaults
        for (ServerHTTProtocol peer : connectedPeers) {
            for (Business res : peer.getBusinessResults(keyword)) {
                if (!results.contains(res)) {
                    results.add(res);
                }
            }
        }

        // send response back to client
        getClient().receiveBusinessResponse(results);
    }

    public void processInfoRequest(String businessName) throws RemoteException, NotBoundException {
        List<Review> results;
        for (Business b : getBusinessVault()) {
            if (b.name.equals(businessName)) {
                results = getReviewResults(b.id);
                System.out.println("Sending results: " + results.size());
                // send response back to client
                getClient().receiveInfoResponse(b, results);
                break;
            }
        }
    }

    // Returns true is data of business is on another peer
    boolean hasPeerLink(String bid) {
        return businessPeerPointerTable.containsKey(bid);
    }

    public void processPostRequest(String businessName, String review) throws RemoteException, NotBoundException {
        for (String bid : vault.getBusinessIDs()) {
            Business b = vault.getBusiness(bid);
            if (b.name.equals(businessName)) {
                Review r = new Review(b.id, 3, review);
                executePost(b, r);
                break;
            }
        }
    }

    boolean isBusinessInVault(String id) {
        for (String bid : vault.getBusinessIDs()){
            if (bid.equals(id)) {
                return true;
            }
        }
        return false;
    }

    public ClientHTTProtocol getClient() throws RemoteException, NotBoundException {
        // Get the registry and lookup our server
        Registry reg = LocateRegistry.getRegistry();
        return (ClientHTTProtocol) reg.lookup("HTTPClient");
    }

    public PeerDiscoveryProtocol contactDiscoveryServer() throws RemoteException, NotBoundException {
        // Contact discovery server
        Registry reg = LocateRegistry.getRegistry();
        return (PeerDiscoveryProtocol) reg.lookup("DiscoveryServer");
    }

    public static void main(String[] args) throws RemoteException, NotBoundException {
        new YelpInterface();
    }
}
